﻿using challenge_net_gabriel.Entities;
using challenge_net_gabriel.Repository.Interfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace challenge_net_gabriel.Repository
{
    public class ClientRepository : BaseRepository, IClientRepository
    {
        public void Add(Client entity)
        {
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@Name", entity.Name);
                parameters.Add("@LastName", entity.LastName);
                parameters.Add("@Document", entity.Document);
                parameters.Add("@BirthDate", entity.BirthDate);
                parameters.Add("@Profession", entity.Profession);
                SqlMapper.Execute(con, "AddClient", parameters, null, null, CommandType.StoredProcedure);
            }catch(Exception ex)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@ClientId", id);
                SqlMapper.Execute(con, "DeleteClientById", parameters, null, null, CommandType.StoredProcedure);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Client Get(int id)
        {
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@ClientId", id);
                return SqlMapper.Query<Client>(con, "GetClientById", parameters, null, true, null, CommandType.StoredProcedure).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IList<Client> Get()
        {
            IList<Client> customerList = SqlMapper.Query<Client>(con, "GetAllClients", CommandType.StoredProcedure).ToList();
            return customerList;
        }

        public void Update(Client entity)
        {
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@Id", entity.Id);
                parameters.Add("@Name", entity.Name);
                parameters.Add("@LastName", entity.LastName);
                parameters.Add("@Document", entity.Document);
                parameters.Add("@BirthDate", entity.BirthDate);
                parameters.Add("@Profession", entity.Profession);
                SqlMapper.Execute(con, "UpdateClient", parameters, null, null, CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
