﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace challenge_net_gabriel.UI
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Serve the files Default.htm, default.html, Index.htm, Index.html
            // by default (in this order), i.e., without having to explicitly qualify the URL.
            // For example, if your endpoint is http://localhost:3012/ and wwwroot directory
            // has Index.html, then Index.html will be served when someone hits
            // http://localhost:3012/
            app.UseDefaultFiles();

            // Enable static files to be served. This would allow html, images, etc. in wwwroot
            // directory to be served. 
            app.UseStaticFiles();
        }
    }
}
