(function () {
    'use strict';
    angular
        .module("challenge")
        .factory("authInterceptor", authInterceptor);
    authInterceptor.$inject = ['$rootScope', '$q'];
    function authInterceptor($rootScope, $q) {
        return {
            request: function (config) {
                
                angular.element(".loading").show();
                return config;
            },
            response: function (response) {
                if (response.status === 403) {
                    console.log("Proibido");
                }
                else if (response.status === 405) {
                    console.log("erro");
                }
                else if (response.status == 401) {
                    console.log("Proibido");
                }
                angular.element(".loading").hide();
                return response;
            }
        };
    }
})();
(function () {
    'use strict';
    angular
        .module("challenge")
        .config(interceptorPush);
    interceptorPush.$inject = ['$httpProvider'];
    function interceptorPush($httpProvider) {
        $httpProvider.interceptors.push('authInterceptor');
    }
})();