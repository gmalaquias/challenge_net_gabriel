angular
    .module("challenge")
    .config(function ($routeProvider, $locationProvider, $httpProvider, $translateProvider) {
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }
        $routeProvider
            .when('/', {
                templateUrl: '/app/pages/client/index/index.html',
                controller: 'IndexController'
            })
            .otherwise({
                redirectTo: "/"
            });
        $locationProvider.html5Mode(true);

        $translateProvider.useStaticFilesLoader({
            prefix: 'app/translations/',
            suffix: '.json',
        });

        $translateProvider.preferredLanguage('pt');
    })
app.run(['$route', '$rootScope', function ($route, $rootScope) {
    $route.reload();
}]);