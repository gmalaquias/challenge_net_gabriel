var app = angular.module('challenge', ['ngRoute', 'ngAnimate', 'ngSanitize', 'ui.mask', 'pascalprecht.translate', '720kb.datepicker']);
app.constant("constants", {
    "API": 'https://challengenetgabrielapi.azurewebsites.net',
});
app.filter('cpf', function () {
    return function (input) {
        var str = input + '';
        if (str.length <= 11) {
            str = str.replace(/\D/g, '');
            str = str.replace(/(\d{3})(\d)/, "$1.$2");
            str = str.replace(/(\d{3})(\d)/, "$1.$2");
            str = str.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
        }
        return str;
    };
});