﻿app.service('ClientService', ['$http', 'constants', function ($http, constants) {

    this.getAll = function (sucess, error) {
        var path = constants.API + '/api/Clients';

        return $http.get(path)
            .success(sucess)
            .error(error);
    };

    this.get = function (id, sucess, error) {
        var path = constants.API + '/api/Clients/' + id;

        return $http.get(path)
            .success(sucess)
            .error(error);
    };

    this.remove = function (id, sucess, error) {
        var path = constants.API + '/api/Clients/' + id;

        return $http.delete(path)
            .success(sucess)
            .error(error);
    };

    this.add = function (client, sucess, error) {
        var path = constants.API + '/api/Clients/';

        return $http.post(path, client, {
            headers: { 'Content-Type': 'application/json' }
        })
            .success(sucess)
            .error(error);
    };

    this.update = function (client, sucess, error) {
        var path = constants.API + '/api/Clients/' + client.id;

        return $http.put(path, client, {
            headers: { 'Content-Type': 'application/json' }
        })
            .success(sucess)
            .error(error);
    };

}]);