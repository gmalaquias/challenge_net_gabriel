﻿app.controller('IndexController', ['$scope', '$http', '$translate', 'ClientService', function ($scope, $http, $translate, ClientService) {

    $scope.client = {};
    $scope.errorRequired = false;
    $scope.AddSuccess = false;
    $scope.DeleteSuccess = false;

    $scope.init = function () {
        $scope.errorRequired = false;
        $scope.AddSuccess = false;
        $scope.DeleteSuccess = false;

        $scope.client = {
            "id": "0",
            "name": "",
            "lastName": "",
            "document": "",
            "birthDate": "",
            "profession": ""
        };
    }
        

    $scope.search = "";

    //Programador, Analista, Gerente, Estagiário e QA
    $scope.occupations = [
        { "id": null, "description": "" },
        { "id": 1, "description": "PROGRAMMER" },
        { "id": 2, "description": "ANALYST" },
        { "id": 3, "description": "MANAGER" },
        { "id": 4, "description": "TRAINEE" },
        { "id": 5, "description": "QA" }
    ];

    $scope.costumers = [];

    $scope.getAll = function () {
        console.log("...");
        ClientService.getAll(function (data) { $scope.costumers = data; console.log(data) }, function () { });
    }

    $scope.edit = function (id) {
        ClientService.get(id, function (data) {
            $scope.client = data;
            $scope.client.profession = $scope.client.profession.toString();
            var data = new Date($scope.client.birthDate);
            $scope.client.birthDate = data.toLocaleString("pt-BR");

        }, function () { });
    }

    $scope.save = function () {
        $scope.AddSuccess = false;
        $scope.DeleteSuccess = false;
        $scope.errorRequired = false;
        var client = $scope.client;
        if (client.name == "" || client.lastName == "" || client.birthDate == "" || client.document == "") {
            $scope.errorRequired = true;
            return;
        }

        $scope.client.birthDate = new Date($scope.client.birthDate.replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3")).toISOString();
        console.log(parseInt($scope.client.id));
        if (parseInt($scope.client.id) <= 0) {
            console.log("Salving");
            ClientService.add($scope.client, function () { }, function () { });
        }
        else {
            console.log("Updating");
            ClientService.update($scope.client, function () { }, function () { });
        }

        setTimeout(function () { $scope.getAll(); }, 1000);
        
        $scope.init();
        $scope.AddSuccess = true;
    }

    $scope.remove = function (client) {
        ClientService.remove(client.id, function () {
            var i = $scope.costumers.indexOf(client);
            $scope.costumers.splice(i, 1); 
        }, function () { });
        $scope.DeleteSuccess = true;
        $scope.AddSuccess = false;
        $scope.errorRequired = false;
    }


    //read records
    $scope.getAll();
    $scope.init();
    

    
}]);