app.controller('PageController', ['$scope', '$translate', function ($scope, $translate) {

    $scope.changeLanguage = function (language) {
        $translate.use(language);
    }

    var lang = navigator.languages ? navigator.languages[0] : (navigator.language || navigator.userLanguage);
    if (lang != "pt-BR")
        $translate.use("en");

}]);