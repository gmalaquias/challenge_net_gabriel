﻿using challenge_net_gabriel.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace challenge_net_gabriel.Repository.Interfaces
{
    public interface IClientRepository
    {
        Client Get(int id);
        void Add(Client entity);
        void Delete(int id);
        void Update(Client entity);
        IList<Client> Get();
    }
}
