﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using challenge_net_gabriel.Business.Interfaces;
using challenge_net_gabriel.Entities;
using Microsoft.AspNetCore.Mvc;

namespace challenge_net_gabriel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientsController : ControllerBase
    {
        IClientManager _clientManager;

        public ClientsController(IClientManager clientManager)
        {
            _clientManager = clientManager;
        }

        [HttpGet]
        public IEnumerable<Client> Get()
        {
            return _clientManager.GetAllClient();
        }

        [HttpGet("{id}")]
        public Client Get(int id)
        {
            return _clientManager.GetClientById(id);
        }

        [HttpPost]
        public void Post([FromBody] Client client)
        {
            _clientManager.AddClient(client);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Client client)
        {
            client.Id = id;
            _clientManager.UpdateClient(client);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _clientManager.DeleteClient(id);
        }
    }
}
