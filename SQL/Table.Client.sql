

CREATE TABLE Client (
	Id int not null primary key identity(1,1),
	Name varchar(30) not null,
	LastName varchar(100) not null,
	Document varchar(11) not null,
	BirthDate DATE not null,
	Age int not null, 
	Profession int null
);