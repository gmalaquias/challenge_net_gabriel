-- =======================================================
-- Create Stored Procedure Template for Azure SQL Database
-- =======================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Malaquias, Gabriel
-- Create Date: 31/08/2018
-- Description: return all clients
-- =============================================
CREATE PROCEDURE AddClient
	@Name varchar(30),
	@LastName varchar(100),
	@Document varchar(11),
	@BirthDate DATE,
	@Profession int
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

	DECLARE @Age INT = FLOOR(DATEDIFF(DAY, @BirthDate, GETDATE()) / 365.25);

    INSERT INTO Client (Name,LastName,Document,BirthDate,Age,Profession) VALUES (@Name, @LastName, @Document, @BirthDate, @Age, @Profession);
END
GO
