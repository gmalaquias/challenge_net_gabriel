-- =======================================================
-- Create Stored Procedure Template for Azure SQL Database
-- =======================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Malaquias, Gabriel
-- Create Date: 31/08/2018
-- Description: return all clients
-- =============================================
CREATE PROCEDURE GetAllClients
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

    SELECT Id, Name, LastName, Document, BirthDate, Age, Profession FROM Client;
END
GO
