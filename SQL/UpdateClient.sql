-- =======================================================
-- Create Stored Procedure Template for Azure SQL Database
-- =======================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Malaquias, Gabriel
-- Create Date: 31/08/2018
-- Description: return all clients
-- =============================================
CREATE PROCEDURE UpdateClient
	@Id int,
	@Name varchar(30),
	@LastName varchar(100),
	@Document varchar(11),
	@BirthDate DATE,
	@Profession int
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

	DECLARE @Age INT = FLOOR(DATEDIFF(DAY, @BirthDate, GETDATE()) / 365.25);

    UPDATE Client SET Name=@Name, LastName=@LastName,Document=@Document,BirthDate=@BirthDate,Age=@Age,Profession=@Profession WHERE Id = @Id;
END
GO
