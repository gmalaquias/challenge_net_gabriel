﻿using System;
using System.Collections.Generic;
using System.Text;

namespace challenge_net_gabriel.Entities
{
    public class Client
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Document { get; set; }

        public DateTime BirthDate { get; set; }

        public int Age { get; set; }

        public int? Profession { get; set; }

    }
}
