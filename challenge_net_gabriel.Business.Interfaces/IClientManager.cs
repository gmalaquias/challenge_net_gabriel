﻿using challenge_net_gabriel.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace challenge_net_gabriel.Business.Interfaces
{
    public interface IClientManager
    {
        bool AddClient(Client client);
        bool UpdateClient(Client client);
        bool DeleteClient(int id);
        IList<Client> GetAllClient();
        Client GetClientById(int id);
    }
}
