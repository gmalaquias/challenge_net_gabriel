﻿using challenge_net_gabriel.Business.Interfaces;
using challenge_net_gabriel.Entities;
using challenge_net_gabriel.Repository.Interfaces;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace challenge_net_gabriel.Business
{
    public class ClientManager : IClientManager
    {
        IClientRepository _clientRepository;

        public ClientManager(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        public bool AddClient(Client client)
        {
            try
            {
                _clientRepository.Add(client);
                return true;
            }catch(Exception e)
            {
                return false;
            }
        }

        public bool DeleteClient(int id)
        {
            try
            {
                _clientRepository.Delete(id);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IList<Client> GetAllClient()
        {
            return _clientRepository.Get();
        }

        public Client GetClientById(int id)
        {
            return _clientRepository.Get(id);
        }

        public bool UpdateClient(Client client)
        {
            try
            {
                _clientRepository.Update(client);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}