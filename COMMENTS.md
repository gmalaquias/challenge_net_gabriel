# Explicação rápida da decisão arquitetura utilizada e o porquê
	- Optei por separar as camadas de negócio e da persistência de dados, para uma melhor escalabilidade do sistema. Também separei as interfaces, para conseguir trabalhar com injeção de dependência. 
# Lista de bibliotecas de teceiros utilizadas
	- Dapper - StackOverflow;
	- Angular Translate;
	- Angular-ui-mask;
	- Font awesome.
# O que você melhoraria se tivesse mais tempo
    - Validação de CNPJ;
	- Validação de Data válida;
	- Tratar todos os campos no backend;
	- Melhorar o "estilo" do front-end;
	- Adicionar autenticação na API;
	- Paginação dos dados.
# Quais requisitos obrigatórios não foram entregues e o porquê 
	- Não criei uma tela especifica para visualização, já que optei em fazer a listagem e a edição na mesma página.
		